export default class Debouncer {
  private timer: number = 0;

  public constructor(
    private timeout: number = 1000) { }

  public invoke(callback: (...params: any[]) => any, ...args: any[]): void {
    this.cancel();
    this.timer = window.setTimeout(callback, this.timeout, ...args);
  }

  public cancel(): void {
    clearTimeout(this.timer);
  }
}
