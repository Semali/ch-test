import * as React from 'react';
import { inject, observer } from 'mobx-react';
import { block } from 'bem-cn';

import { UserStore } from '~/stores/userStore';
import autobind from 'autobind-decorator';
import { Locale } from '~/services/api/dto/clientInfo';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as Icons from '@fortawesome/free-solid-svg-icons';
import { Dropdown } from 'react-bootstrap';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import NavDropdown from 'react-bootstrap/NavDropdown';

const cn = block('app-language-switch');

export interface ILanguageSwitch {
  user?: UserStore;
  className?: string;
  alignRight?: boolean;
  icon?: keyof typeof Icons;
}

const languages: {[key in Locale]: string} = {
  en: 'ENG',
  ru: 'RU',
};

@inject('user')
@observer
export class LanguageSwitch extends React.Component<ILanguageSwitch> {
  public render() {
    const { alignRight, icon } = this.props;
    const iconProp: IconProp = Icons[icon] as Icons.IconDefinition;

    return (
      <NavDropdown
        title={<FontAwesomeIcon icon={iconProp}/>}
        id="basic-nav-dropdown"
        alignRight={alignRight}
        className={cn()}
      >
        {Object.keys(languages).map((l: Locale) => (
          <Dropdown.Item
            key={l}
            onClick={this.handleClick}
            data-lang={l}
          >
            {languages[l]}
          </Dropdown.Item>
        ))}
      </NavDropdown>
    );
  }

  @autobind
  private handleClick({ currentTarget }: React.MouseEvent<HTMLButtonElement>) {
    const { user: { setLocale } } = this.props;

    setLocale(currentTarget.dataset.lang as Locale);
  }
}
