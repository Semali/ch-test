export type EmptyChecker<T> = (value: T) => boolean;

const RE_SPACES = /^\s+$/;

/** Общая проверка значения на пустоту  */
export const common: EmptyChecker<any> = (value: any) => (!value && value !== false && value !== 0)
  || value.length === 0
  || (typeof value === 'string' && RE_SPACES.test(value));
