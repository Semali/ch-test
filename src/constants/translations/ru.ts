/* tslint:disable:max-line-length*/
/* tslint:enable:object-literal-sort-keys */

const translations = Object.freeze({
  'header.home': 'На главную',

  'dashboard.hello': 'Добро пожаловать',
  'dashboard.note.title': 'Заголовок',

  'edit': 'Редактировать',
  'create': 'Создать',
  'close': 'Закрыть',
  'save': 'Сохранить',
});

export default translations;
