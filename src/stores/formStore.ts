import { action, computed, observable } from 'mobx';

import { log } from '~/services/log';
import { Validator } from '~/services/form/validator';

export interface IField<T = any> {
  initialValue: T;
  defaultValue?: T;
  validators?: Array<Validator<T>>;
  [key: string]: any;
}

export interface IFields {
  [key: string]: IField;
}

export interface IFormValues {
  [key: string]: any;
}

export interface IFormErrors {
  [key: string]: string;
}

type IStoreFields<T extends IFields> = Partial<T> & IFields;

type IStoreErrors<T extends IFields> = Partial<{
  [key in keyof T]: string;
}> & IFormErrors;

type IStoreValues<T extends IFields> = Partial<{
  [key in keyof T]: T[key]['initialValue'];
}> & IFormValues;

export function formConfig<T extends IFields>(config: T): T {
  return config;
}

export class FormStore<T extends IFields = {}> {
  /**
   * Конфигурация полей формы с указанием обязательных полей и валидаторов.
   */
  public fields: IStoreFields<T>;

  /**
   * Значения полей формы.
   */
  @observable
  public values: IStoreValues<T>;

  /**
   * Ошибки полей формы.
   */
  @observable
  public errors: IStoreErrors<T>;

  @observable
  protected skipValidation: Array<keyof T | any> = [];

  /**
   * Флаг корректного заполнения формы.
   */
  @computed
  public get isValid(): boolean {
    return Object.keys(this.fields)
      .filter(name => !this.skipValidation.includes(name))
      .every(name => {
        const field = this.fields[name];
        const value = this.values[name];

        return (field.validators || []).every((validate) => !validate(value, this.values));
      });
  }

  constructor(fields: T) {
    this.setConfig(fields, true);
  }

  /**
   * Задает текущий конфиг формы.
   * @param fields конфигурация полей формы
   * @param init
   */
  @action.bound
  public setConfig(fields: T, init: boolean = false): void {
    this.fields = fields;
    this.reset(init);
  }

  /**
   * Сбрасывает ошибки и устанавливает значения полей формы значениями по умолчанию.
   */
  @action.bound
  public reset(init: boolean = false) {
    const values: IStoreValues<T> = Object.keys(this.fields)
      .reduce((obj, name) => ({
        ...obj,
        [name]: init
          ? this.fields[name].initialValue
          : this.fields[name].defaultValue || this.fields[name].initialValue,
      }), {});

    if (JSON.stringify(this.values) !== JSON.stringify(values)) {
      if (!this.values) {
        this.values = values;
      } else {
        // need to observe values on reset
        Object.keys(values).forEach(key => this.values[key] = values[key]);
      }
    }

    this.resetErrors();
  }

  /**
   * Сбрасывает ошибки.
   */
  @action.bound
  public resetErrors() {
    const errors = Object.keys(this.fields).reduce((obj, name) => ({...obj, [name]: ''}), {});

    if (JSON.stringify(this.errors) !== JSON.stringify(errors)) {
      this.errors = errors;
    }
  }

  @action.bound
  public setFieldValue<K extends keyof T | string>(name: K, value: any) {
    this.values[name] = value;

    if (!this.fields[name]) {
      log(`The field "${name}" is not defined among the fields: ${Object.keys(this.fields).join(', ')}`);
    }
  }

  @action.bound
  public resetFieldValue(name: keyof T) {
    this.setFieldValue(name, this.fields[name].defaultValue || this.fields[name].initialValue);
  }

  @action.bound
  public setValues(values: IStoreValues<T>) {
    if (values) {
      Object.keys(values).forEach((name) => this.setFieldValue(name, values[name]));
    }
  }

  @action.bound
  public setErrors(errors: IStoreErrors<T>) {
    this.errors = errors;
  }

  @action.bound
  public setSkipValidation(fields: Array<keyof T>) {
    this.skipValidation = fields;
  }
}
