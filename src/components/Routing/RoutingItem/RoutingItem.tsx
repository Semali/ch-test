import * as React from 'react';
import { RouteProps } from 'react-router';

import PrivateRoute from '../PrivateRoute/index';

export interface IRoutingItem extends RouteProps {
  routeComponent?: React.ComponentClass<RouteProps>;
  breadcrumbs?: boolean;
}

export const RoutingItem: React.FunctionComponent<IRoutingItem> = ({routeComponent = PrivateRoute, ...routeProps}) => {
  const RouteComponent = routeComponent;

  return (
    <RouteComponent {...routeProps} />
  );
};
