interface IAppConfig {
  /**
   * base url
   */
  baseUrl: string;
  /**
   * REST API context
   */
  apiUrl: string;
  /**
   * Development information visibility flag
   */
  showDevInfo: boolean;
}

const appConfig: IAppConfig = { ...(window as any).APP_CONFIG };

export default appConfig;
