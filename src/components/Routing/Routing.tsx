import * as React from 'react';
import { Switch } from 'react-router';

import RoutingItem, { IRoutingItem } from './RoutingItem';

export interface IRouting {
  routes: IRoutingItem[];
}

export const Routing: React.FunctionComponent<IRouting> = ({ routes }) => (
  <Switch>
    {routes.map((props) => <RoutingItem key={props.path as string} {...props} />)}
  </Switch>
);
