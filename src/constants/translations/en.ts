/* tslint:disable:max-line-length*/
/* tslint:enable:object-literal-sort-keys */

const translations = Object.freeze({
  'header.home': 'Home',

  'dashboard.hello': 'Welcome',
  'dashboard.note.title': 'Title',

  'edit': 'Edit',
  'create': 'Create',
  'close': 'Close',
  'save': 'Save',
});

export default translations;
