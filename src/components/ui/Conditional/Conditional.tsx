import React from 'react';
import Loader from '../Loader';
import ErrorBlock from '../ErrorBlock';

interface IConditionalProps {
  error: React.ReactNode;
  loading: boolean;
}

export const Conditional: React.FC<IConditionalProps> = (props) => {
  const {error, loading, children} = props;

  if (error) {
    return (
      <ErrorBlock>
        {error}
      </ErrorBlock>
    );
  }

  if (loading) {
    return (
      <Loader/>
    );
  }

  return children as JSX.Element;
};
