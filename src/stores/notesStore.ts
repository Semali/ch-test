import { action, observable } from 'mobx';
import { logError } from '~/services/log';
import INote from '~/services/api/dto/noteInfo';
import { deleteNote, getList } from '~/services/api/note';

export class NotesStore {
  @observable
  public isBusy: boolean = false;

  @observable
  public error: boolean = false;

  @observable
  public notesList: INote[] = [];

  @observable
  public note: INote;

  @action.bound
  public async getList() {
    this.isBusy = true;

    try {
      this.notesList = await getList();
    } catch (e) {
      logError(e);
    } finally {
      this.isBusy = false;
    }
  }

  @action.bound
  public async deleteNote(id: number | string) {
    this.isBusy = true;

    try {
      await deleteNote(id);
    } catch (e) {
      logError(e);
    } finally {
      this.isBusy = false;
    }
  }
}
