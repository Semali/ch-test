import { formConfig } from '~/stores/formStore';
import * as v from '~/services/form/validator';

const config = formConfig({
  title: {
    initialValue: '',
    validators: [ v.required() ],
  },
});

export default config;
