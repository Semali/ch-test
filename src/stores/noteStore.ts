import { action, observable } from 'mobx';
import { logError } from '~/services/log';
import INote from '~/services/api/dto/noteInfo';
import { getNote } from '~/services/api/note';
import { NotesStore } from '~/stores/notesStore';

export class NoteStore extends NotesStore {
  @observable
  public note: INote = undefined;

  public constructor(public id: string) {
    super();
  }

  @action.bound
  public init() {
    return Promise.all([
      this.getNote(),
    ]);
  }

  @action.bound
  public async getNote() {
    this.isBusy = true;

    try {
      this.note = await getNote(this.id);
    } catch (e) {
      logError(e);
    } finally {
      this.isBusy = false;
    }
  }
}
