import * as React from 'react';
import { block } from 'bem-cn';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as Icons from '@fortawesome/free-solid-svg-icons';
import { action } from 'mobx';
import Button from 'react-bootstrap/Button';

const cn = block('note-row');

export interface INoteRow {
  id: string | number;
  title: string;
  deleteNote?: (id: string | number) => void;
}

export class NoteRow extends React.PureComponent<INoteRow> {
  public render() {
    const { id, title } = this.props;

    return (
      <tr className={cn()}>
        <td>
          {id}
        </td>
        <td>
          <NavLink to={`/note/${id}`}>{title}</NavLink>
        </td>
        <td>
          <Button
            className={cn('button').toString()}
            variant="link"
            onClick={this.handleClick}
          >
            <FontAwesomeIcon
              icon={Icons['faTimes' as keyof typeof Icons] as Icons.IconDefinition}
            />
          </Button>
        </td>
      </tr>
    );
  }

  @action.bound
  public handleClick() {
    const { id, deleteNote } = this.props;

    if (deleteNote) {
      deleteNote(id);
    }
  }
}
