module.exports = {
  setupTestFrameworkScriptFile: '<rootDir>/src/setupTests.ts',
  roots: [
    '<rootDir>/src'
  ],
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
    '.+\\.(css|styl|less|sass|scss|css.js)$': '<rootDir>/__mocks__/styleMock.js',
  },
  snapshotSerializers: ["enzyme-to-json/serializer"],
  testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$',
  clearMocks: true,
  moduleFileExtensions: [
    'ts',
    'tsx',
    'js',
    'jsx',
    'json',
    'node'
  ],
  testEnvironment: './testEnvironment.js',
  moduleNameMapper: {
    '\~(.*)$': '<rootDir>/src/$1'
  }
};
