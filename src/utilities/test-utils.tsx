import React from 'react';
import { shallow, mount } from 'enzyme';
import { initializeStores, IRootStore } from '~/stores';
import { Provider } from 'mobx-react';
import createMemoryHistory, { MemoryHistoryBuildOptions } from 'history/createMemoryHistory';

interface ITestEnvironmentConfig {
  historyConfig?: MemoryHistoryBuildOptions;
  match?: {
    params: { [key: string]: string },
    isExact: boolean;
    path: string;
    url: string;
  };
  storeMutators?: Array<(allStores: IRootStore) => void>;
}

export function testEnvironmentCreator(opts: ITestEnvironmentConfig = {}) {
  const stores = initializeStores();
  const history = createMemoryHistory(opts.historyConfig);
  const location = history.location;
  const match = opts.match || {
    params: {},
    isExact: false,
    path: '/',
    url: '/',
  };
  if (opts.storeMutators && opts.storeMutators.length > 0) {
    opts.storeMutators.forEach(mutator => mutator(stores));
  }
  return {
    routeComponentProps: {
      history,
      location,
      match,
    },
    stores,
    shallow: (Container: React.ComponentType<any>, props = {}) =>
      shallow(<Container {...props}/>),
    mount: (Container: React.ComponentType, props = {}) =>
      mount(
        <Provider {...stores}>
          <Container {...props}/>
        </Provider>,
      ),
  };
}

export function changeEvent(value: any) {
  return { target: { value } };
}

export function wait(delay: number = 100) {
  return new Promise(resolve => setTimeout(resolve, delay));
}
