import { FormStore } from '~/stores/formStore';
import editNoteFormConfig from '~/components/EditPage/editNoteFormConfig';
import { action, observable } from 'mobx';
import { logError } from '~/services/log';
import INote, { ICreateNote } from '~/services/api/dto/noteInfo';
import { createNote, editNote } from '~/services/api/note';

export class EditNoteStore extends FormStore<typeof editNoteFormConfig> {
  @observable
  public isBusy: boolean = false;

  @observable
  public error: boolean = false;

  constructor() {
    super(editNoteFormConfig);
  }

  @action.bound
  public async createNote(note: ICreateNote) {
    this.isBusy = true;

    try {
      await createNote(note);
    } catch (e) {
      logError(e);
    } finally {
      this.isBusy = false;
    }
  }

  @action.bound
  public async editNote(note: INote) {
    this.isBusy = true;

    try {
      await editNote(note);
    } catch (e) {
      logError(e);
    } finally {
      this.isBusy = false;
    }
  }
}
