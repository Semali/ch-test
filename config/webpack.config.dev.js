const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

// config
const publicPath = '/';

module.exports = (env) => ({
  mode: 'development',
  target: 'web',
  devtool: 'inline-source-map',
  stats: true,
  devServer: {
    historyApiFallback: {
      disableDotRule: true
    },
    open: true,
    hot: true,
    contentBase: './build',
    compress: true,
    port: 9000,
    stats: 'errors-only',
    proxy: {
      '/api/*': {
        target: 'http://private-9aad-note10.apiary-mock.com/',
        secure: false,
        changeOrigin: true,
        pathRewrite: {"^/api" : ""},
      },
    }
  },
  entry: [path.resolve('src/index.tsx')],
  output: {
    publicPath,
    filename: 'static/js/[name].[hash].js',
    chunkFilename: 'static/js/[name].[chunkhash:8].chunk.js',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.json'],
    alias: {
      '~': path.resolve('src/')
    },
    // https://github.com/andreypopp/autobind-decorator#es5-and-uglify-users
    mainFields: ['main'],
  },
  module: {
    strictExportPresence: true,
    rules: [
      {
        test: /\.ts$/,
        enforce: 'pre',
        use: [
          {
            loader: 'tslint-loader',
            options: {
              emitErrors: true,
            }
          }
        ]
      },
      {
        oneOf: [
          {
            loader: 'babel-loader',
            test: /\.js$/,
            include: /node_modules/,
            query: {
              plugins: ['lodash'],
              presets: ['@babel/preset-env'],
            }
          },
          {
            test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/, /\.svg$/],
            loader: require.resolve('url-loader'),
            options: {
              limit: 10000,
              publicPath: `${publicPath}static/media/`,
              outputPath: 'static/media/',
              name: '[name].[hash:8].[ext]',
            },
          },
          {
            test: /\.tsx?$/,
            loader: 'awesome-typescript-loader',
            options: {
              useCache: true,
              useTranspileModule: true,
              forceIsolatedModules: true,
            }
          },
          {
            test: /\.(sa|sc|c)ss$/,
            use: [
              'style-loader',
              'css-loader',
              {
                loader: require.resolve('postcss-loader'),
                options: {
                  ident: 'postcss',
                  plugins: () => [
                    require('postcss-flexbugs-fixes'),
                    autoprefixer({
                      browsers: [
                        '>1%',
                        'last 4 versions',
                        'Firefox ESR',
                        'not ie < 9',
                      ],
                      flexbox: 'no-2009',
                    }),
                  ],
                },
              },
              'sass-loader',
            ],
          },
          {
            exclude: [/\.js$/, /\.html$/, /\.json$/],
            loader: require.resolve('file-loader'),
            options: {
              publicPath: `${publicPath}static/media/`,
              outputPath: 'static/media/',
              name: '[name].[hash:8].[ext]',
            },
          },
        ]
      }
    ]
  },
  plugins: [
    new CopyWebpackPlugin(
      [
        { from: 'public' }
      ]
    ),
    new HtmlWebpackPlugin({
      inject: true,
      template: path.resolve('public/index.html'),
      env: {
        publicPath,
      }
    }),
    new webpack.DefinePlugin({
      'process.env': {
        PUBLIC_PATH: JSON.stringify(publicPath),
      }
    }),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new webpack.HotModuleReplacementPlugin(),
  ],
  node: {
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty',
  }
});
