import { action, computed, observable } from 'mobx';
import { default as IClientInfo, Locale } from '~/services/api/dto/clientInfo';
import { getLanguage } from '~/utilities/navigator';
import { ITranslations } from '~/services/i18';

export class UserStore {
  @observable
  public clientInfo: IClientInfo;

  @observable
  public isBusy: boolean = false;

  @observable
  public error: boolean = false;

  @observable
  public changeLanguage: Locale;

  @observable
  public serverTranslations: ITranslations = null;

  @computed
  public get locale(): Locale {
    return this.changeLanguage || getLanguage();
  }

  @action.bound
  public setLocale(locale: Locale) {
    this.changeLanguage = locale;
    localStorage.setItem('locale', locale);
  }
}
