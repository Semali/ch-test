import * as React from 'react';
import { block } from 'bem-cn';
import { Spinner } from 'react-bootstrap';

const cn = block('app-loader');

export const Loader: React.FC = () => (
  <div className={cn()}>
    <Spinner className={cn('spinner').toString()} animation="border" />
  </div>
);
