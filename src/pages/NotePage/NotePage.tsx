import * as React from 'react';
import { block } from 'bem-cn';
import { inject, observer } from 'mobx-react';
import { Container, Row, Button } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';
import L10n from '~/components/L10n';
import { Injected } from '~/stores';
import Conditional from '~/components/ui/Conditional';
import { action, observable } from 'mobx';
import { NoteStore } from '~/stores/noteStore';
import EditPage from '~/components/EditPage';

const cn = block('note-page');

export interface INotePage {
  id: string;
}

type INotePageProps = RouteComponentProps<INotePage> & Injected<'notes'>;

@inject('notes')
@observer
export class NotePage extends React.Component<INotePageProps> {
  @observable
  public showModal: boolean = false;

  @observable
  private store = new NoteStore(this.props.match.params.id);

  public componentDidMount(): void {
    this.store.init();
  }

  public render() {
    const { isBusy, error, note } = this.store;

    return (
      <div className={cn()}>
        <Conditional
          loading={isBusy && !note}
          error={error}
        >
          {this.renderContent()}
        </Conditional>
      </div>
    );
  }

  public renderContent() {
    const { note } = this.store;
    const { match: { params: { id } } } = this.props;

    if (!note) {
      return null;
    }

    return (
      <Container>
        <Row>
          <div className={cn('content')}>
            {note.title}
          </div>
        </Row>
        <Row>
          <Button onClick={this.openModal}>
            <L10n k={'edit'}/>
          </Button>
          <EditPage
            id={id}
            content={note.title}
            open={this.showModal}
            onClose={this.closeModal}
            onSuccess={this.store.getNote}
          />
        </Row>
      </Container>
    );
  }

  @action.bound
  private openModal() {
    this.showModal = true;
  }

  @action.bound
  private closeModal() {
    this.showModal = false;
  }
}
