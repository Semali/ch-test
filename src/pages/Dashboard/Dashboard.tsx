import * as React from 'react';
import { block } from 'bem-cn';
import { inject, observer } from 'mobx-react';
import { Container, Row, Col, Button, Table } from 'react-bootstrap';
import L10n from '~/components/L10n';
import { Injected } from '~/stores';
import Conditional from '~/components/ui/Conditional';
import NoteRow from '~/pages/Dashboard/NoteRow';
import { action, observable } from 'mobx';
import EditPage from '~/components/EditPage';

const cn = block('dashboard-page');

export type IDashboard = {}
  & Injected<'notes'>;

@inject('notes')
@observer
export class Dashboard extends React.Component<IDashboard> {
  @observable
  public showModal: boolean = false;

  public componentDidMount(): void {
    const { notes } = this.props;

    notes.getList();
  }

  public render() {
    const { notes: { isBusy, error, notesList, getList } } = this.props;

    return (
      <div className={cn()}>
        <Container fluid>
          <Row>
            <Col className={cn('greeting').toString()}>
              <L10n k={'dashboard.hello'}/>
            </Col>
            <Col className="text-right">
              <Button
                className={cn('btn-create').toString()}
                variant="link"
                onClick={this.openModal}
              >
                <L10n k={'create'}/>
              </Button>
            </Col>
          </Row>
        </Container>
        <Container fluid>
          <Conditional
            loading={isBusy}
            error={error}
          >
            <Table responsive>
              <thead>
                <tr>
                  <th>#</th>
                  <th><L10n k={'dashboard.note.title'}/></th>
                  <th/>
                </tr>
              </thead>
              <tbody>
                {
                  notesList.map(note => (
                    <NoteRow
                      key={note.id}
                      id={note.id}
                      title={note.title}
                      deleteNote={this.deleteNote}
                    />
                  ))
                }
              </tbody>
            </Table>
          </Conditional>
          <EditPage
            open={this.showModal}
            onClose={this.closeModal}
            onSuccess={getList}
          />
        </Container>
      </div>
    );
  }

  @action.bound
  public openModal() {
    this.showModal = true;
  }

  @action.bound
  public closeModal() {
    this.showModal = false;
  }

  @action.bound
  public deleteNote(id: number | string) {
    const { notes } = this.props;

    notes.deleteNote(id);
  }
}
