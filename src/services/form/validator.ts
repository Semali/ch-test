import * as isEmpty from './emptyChecker';

const REQUIRED = 'Пожалуйста, заполните поле';
const REQUIREDPHONENUMBER = 'Пожалуйста, введите корректный номер телефона';

export type Validator<T> = (value: T, values?: { [key: string]: any }, ...arg: any) => string;

export const required = (checkEmpty: isEmpty.EmptyChecker<any> = isEmpty.common): Validator<any> =>
  (value: any) => checkEmpty(value) ? REQUIRED : '';
