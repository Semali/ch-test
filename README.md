## Prerequisites
1. Clone a repository.
2. In the project directory execute `npm install` or `yarn install`

## Run development server
Run `npm start` or `yarn start` for a dev server.
App will be opened on `http://localhost:9000/`.
The app will automatically reload if you change any of the source files.

## Build
Run `npm build` or `yarn build` to build the project.

## Running unit tests
Run `npm test` or `yarn test` to execute the unit tests via [Jest](https://jestjs.io/) and [Enzyme](https://github.com/airbnb/enzyme).
