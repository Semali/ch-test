export type Locale = 'en' | 'ru';

export default interface IClientInfo {
  locale: Locale;
}
