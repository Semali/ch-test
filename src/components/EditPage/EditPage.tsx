import * as React from 'react';
import { block } from 'bem-cn';
import { inject, observer } from 'mobx-react';
import L10n from '~/components/L10n';
import { Injected } from '~/stores';
import { action } from 'mobx';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { Input } from '~/components/FormControl';

const cn = block('edit-page');

export interface IEditPage {
  open: boolean;
  id?: string | number;
  content?: string;
  onClose(): void;
  onSuccess(): void;
}

type IEditPageProps = IEditPage & Injected<'notes', 'editNote'>;

@inject('notes', 'editNote')
@observer
export class EditPage extends React.Component<IEditPageProps> {
  public componentDidMount(): void {
    const { id, content, editNote } = this.props;

    if ((id || id === 0) && content) {
      editNote.setFieldValue('title', content);
    } else {
      editNote.resetFieldValue('title');
    }
  }

  public render() {
    const { open, id, editNote } = this.props;

    return (
      <Modal
        className={cn().toString()}
        show={open}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        onHide={this.handleClose}
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            {
              (id || id === 0) ?
                <L10n k={'edit'}/> :
                <L10n k={'create'}/>
            }
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Input
            store={editNote}
            name="title"
          />
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.handleClose}>
            <L10n k={'close'}/>
          </Button>
          <Button
            onClick={this.handleSuccess}
            disabled={!editNote.isValid}
          >
            <L10n k={'save'}/>
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }

  @action.bound
  private async handleSuccess() {
    const { editNote, id, onClose, onSuccess } = this.props;

    if (id || id === 0) {
      await editNote.editNote({
        id,
        title: editNote.values.title,
      });
    } else {
      await editNote.createNote({
        title: editNote.values.title,
      });
    }

    if (onSuccess) {
      await onSuccess();
    }

    if (onClose) {
      onClose();
    }
  }

  @action.bound
  private handleClose() {
    const { onClose } = this.props;

    if (onClose) {
      onClose();
    }
  }
}
