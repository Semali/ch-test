import * as React from 'react';
import { block } from 'bem-cn';
import { observer } from 'mobx-react';
import LanguageSwitch from '~/components/LanguageSwitch';
import { NavLink } from 'react-router-dom';
import L10n from '~/components/L10n';

const cn = block('app-header');

@observer
export class Header extends React.Component {
  public render() {
    return (
      <div className={cn()}>
        <div className={cn('navbar')}>
          <div className={cn('navbar-left')}>
            <div className={cn('navbar-logo')}>
              <NavLink to="/"><L10n k={'header.home'}/></NavLink>
            </div>
          </div>
          <div className={cn('navbar-right')}>
            <LanguageSwitch
              alignRight={true}
              icon={'faCog'}
            />
          </div>
        </div>
      </div>
    );
  }
}
