import { Locale } from '~/services/api/dto/clientInfo';

export function getLanguage(): Locale {
  if (localStorage.getItem('locale') as Locale) {
    return localStorage.getItem('locale') as Locale;
  }
  switch (navigator.language) {
    case 'ru-RU':
      return 'ru';
    default:
      return 'en';
  }
}
