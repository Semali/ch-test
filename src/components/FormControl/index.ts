import { FormControl, bindControl } from './FormControl';

import InputControl from '~/components/ui/Input';

export { IFormField } from './FormControl';

export const Generic = FormControl;

export const Input = bindControl(InputControl);
