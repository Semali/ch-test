import { observer } from 'mobx-react';
import * as React from 'react';
import { Route } from 'react-router';

@observer
export class PrivateRoute extends React.Component<any> {

  public render() {
    const { ...routeProps } = this.props;

    return (
      <>
        <Route {...routeProps} />
      </>
    );
  }
}
