import moment from 'moment';
import 'moment/locale/ru';
import 'moment/locale/en-gb';

import en from '~/constants/translations/en';
import ru from '~/constants/translations/ru';
import { Locale } from '~/services/api/dto/clientInfo';
import { LocaleKey } from '~/constants/language';
import formatter from 'format-number';

export interface ITranslations {
  en: { [key: string]: string | string[] };
  ru: { [key: string]: string | string[] };
}

export interface IFormatNumberOptions {
  integerSeparator: string;
  decimal: string;
}

export interface IFormatNumber {
  en: IFormatNumberOptions;
  ru: IFormatNumberOptions;
}

const translations: ITranslations = { en, ru };

const formatNumber: IFormatNumber = {
  en: {
    integerSeparator: ',',
    decimal: '.',
  },
  ru: {
    integerSeparator: ' ',
    decimal: ',',
  },
};

export function translate(lang: Locale, key: LocaleKey, options?: any, failIfNotFound?: boolean): string {
  let text: any = translations[lang][key];
  let plural: string | string[];
  let pluralText: string;
  let index: number;

  if (failIfNotFound && !text) {
    throw new Error(`Translation for key ${key} was not found`);
  }

  if (text && options) {

    if (options.plural && options.plural.length) {
      plural = translations[lang][options.plural];
    }

    Object.keys(options).forEach(option => {
      if (option !== 'plural') {
        const pattern = `{{${option}}}`;

        if (plural) {
          switch (lang) {
            case 'ru':
              index = getIndexRuPluralTranslation(options[option]);
              break;
            default:
              index = options[option] === 1 ? 0 : 1;
              break;
          }
          pluralText = `${plural[index]}`;
        } else {
          pluralText = '';
        }

        text = text.replace(new RegExp(pattern, 'g'), `${options[option]} ${pluralText}`);
      }
    });
  }

  function getIndexRuPluralTranslation(number: number): number {
    if (number % 100 >= 11 && number % 100 <= 19) {
      return 2;
    }
    switch (number % 10) {
      case 1:
        return 0;
      case 2:
      case 3:
      case 4:
        return 1;
      default:
        return 2;
    }
  }

  return text || key;
}

export function translateVariants(lang: Locale, variants: { [key in Locale]?: string }) {
  const secondLang = lang === 'en' ? 'ru' : 'en';

  return variants[lang] || variants[secondLang] || '';
}

export function parseDate(lang: Locale, value?: any, format?: string, valueFormat?: string, dateInUtc = false): string {
  const locale = lang === 'en' ? 'en-gb' : lang;

  if (dateInUtc) {
    return moment.utc(value, valueFormat).local().locale(locale).format(format);
  }

  return (moment.isMoment(value) ? value : moment(value, valueFormat))
    .locale(locale)
    .format(format);
}

export function localizeNumber(lang: Locale, value?: number, options?: any): string {
  if (!value && value !== 0) {
    return '';
  }

  const formatFn = formatter({ ...options, ...formatNumber[lang] });

  return formatFn(value);
}
