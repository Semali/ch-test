const api =  Object.freeze({
  notes: {
    create: 'notes',
    edit: (id: string | number) => `notes/${id}`,
  },
});

export default api;
