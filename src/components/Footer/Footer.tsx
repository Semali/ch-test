import * as React from 'react';
import { block } from 'bem-cn';

const cn = block('app-footer');

export const Footer: React.FunctionComponent<any> = () => (
  <div className={cn()}>
    <a href="https://www.bankingsoftware.company">
      © 2019 BSC
    </a>
  </div>
);
