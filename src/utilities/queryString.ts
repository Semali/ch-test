import qs from 'qs';

/**
 * Converts object to valid for back-end query string.
 * @param params object that is converted to query string params
 */
export function toQueryString(params: any): string {
  return qs.stringify(params, { indices: false });
}
