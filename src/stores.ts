import { RouterStore } from 'mobx-react-router';

// сторы
import { UserStore } from '~/stores/userStore';
import { NotesStore } from '~/stores/notesStore';
import { EditNoteStore } from '~/stores/EditNoteStore';

export function initializeStores() {
  return {
    user: new UserStore(),
    routing: new RouterStore(),
    notes: new NotesStore(),
    editNote: new EditNoteStore(),
  };
}

const stores = initializeStores();

export type IRootStore = typeof stores;

export type StoreKeys = keyof IRootStore;

export type Injected<
    T extends StoreKeys = undefined,
    U extends StoreKeys = undefined,
    V extends StoreKeys = undefined,
    W extends StoreKeys = undefined,
    > = Partial<{
  [k in T | U | V | W]: typeof stores[k];
}>;

export default stores;
