import * as React from 'react';
import { Alert } from 'react-bootstrap';

export const ErrorBlock: React.FC = ({ children }) => (
  <Alert variant={'danger'}>
    {children}
  </Alert>
);
