import * as React from 'react';
import { computed } from 'mobx';
import { inject, observer } from 'mobx-react';
import { UserStore } from '~/stores/userStore';
import { Locale } from '~/services/api/dto/clientInfo';
import { LocaleKey } from '~/constants/language';
import stores from '~/stores';
import {
  localizeNumber,
  parseDate,
  translate,
  translateVariants,
} from '~/services/i18';

export interface IL10n {
  user?: UserStore;
  type: 'text' | 'date' | 'number' | 'catalog' | 'amount';
  saveHtml?: boolean;
  className?: string;
  k?: LocaleKey;
  options?: any;
  translations?: {[key in Locale]?: string};
  value?: any;
  valueFormat?: string;
  dateInUtc?: boolean;
}

export function t(k: LocaleKey, options?: any) {
  return translate(stores.user.locale, k, options, true);
}

@inject('user')
@observer
export class L10n extends React.Component<IL10n> {
  public static defaultProps: Partial<IL10n> = {
    type: 'text',
    saveHtml: false,
    className: '',
    dateInUtc: false,
  };

  @computed
  private get text(): string {
    const { k = '', options, translations, user: { locale, serverTranslations } } = this.props;

    try {
      return translations ? translateVariants(locale, translations) : translate(locale, k, options, true);
    } catch {
      if (serverTranslations) {
        return serverTranslations[locale][k] as string || k;
      }
      return k;
    }
  }

  @computed
  private get date(): string {
    const { k = 'DD MMMM YYYY', value, valueFormat, user: { locale }, dateInUtc } = this.props;

    return parseDate(locale, value, k, valueFormat, dateInUtc);
  }

  @computed
  private get number(): string {
    const { value, options, user: { locale } } = this.props;

    return localizeNumber(locale, value, options);
  }

  public render() {
    const { type, saveHtml, className } = this.props;
    let content: string;

    switch (type) {
      case 'text':
        content = this.text;
        break;
      case 'date':
        content = this.date;
        break;
      case 'number':
        content = this.number;
        break;
      default:
        content = '';
    }

    return saveHtml
      ? (
        <div className={className} dangerouslySetInnerHTML={{__html: content}}/>
      ) : content;
  }
}
