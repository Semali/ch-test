import * as React from 'react';
import Routing from '~/components/Routing';
import { routes } from './routes';
import Header from '~/components/Header';
import Footer from '~/components/Footer';

export const App: React.FunctionComponent = () => (
  <>
    <Header/>
    <Routing routes={routes}/>
    <Footer/>
  </>
);
