import * as React from 'react';
import autobind from 'autobind-decorator';
import { observer } from 'mobx-react';

import { FormStore } from '~/stores/formStore';

export interface IFormBound {
  store: FormStore;
}

type Control<T> = React.ComponentClass<T> | React.FunctionComponent<T>;

export interface IFormField {
  className?: string;
  label?: React.ReactNode;
  name?: string;
  value?: any;
  disabled?: boolean;
  required?: boolean;
  error?: string | boolean | React.ReactNode;
  onChange?: (value: any, name?: string, event?: any) => void;
  onBlur?: (name?: string, event?: any) => void;
  onFocus?: (name?: string, event?: any) => void;
  resetValue?(name: string, event?: any): void;
}

export interface IFormControlProps<T extends IFormField> {
  name: string;
  cmp?: Control<T>;
  cmpProps?: T;
  store: FormStore;
}

@observer
export class FormControl<T extends IFormField> extends React.Component<IFormControlProps<T>> {
  public render() {
    const { cmp: Cmp, cmpProps = {} as any, store, name } = this.props;

    return (
      <Cmp
        {...cmpProps}
        name={name}
        value={store.values[name]}
        error={(cmpProps && cmpProps.error) || store.errors[name]}
        onChange={this.handleChange}
        resetValue={store.resetFieldValue}
      />
    );
  }

  @autobind
  private handleChange(value: any, fieldName?: string, event?: any) {
    const { store, name, cmpProps } = this.props;

    store.setFieldValue(name, value);

    if (cmpProps && cmpProps.onChange) {
      cmpProps.onChange(value, fieldName, event);
    }
  }
}

// tslint:disable-next-line:max-line-length
export function bindControl<TProps extends IFormField>(control: Control<TProps>): React.FunctionComponent<TProps & IFormBound> {
  return (props) => {
    const { store, ...rest } = props as any;

    return (
      <FormControl<TProps>
        store={store}
        cmp={control}
        cmpProps={rest}
        name={props.name}
      />
    );
  };
}
