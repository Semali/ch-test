export function log(...args: any[]): void {
  if (process.env.NODE_ENV === 'development') {
    console.warn(...args);
  }
}

export function logError(...args: any[]): void {
  if (process.env.NODE_ENV === 'development') {
    console.error(...args);
  }
}
