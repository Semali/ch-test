import { IRoutingItem } from '../Routing/RoutingItem';
import Dashboard from '~/pages/Dashboard';
import NotePage from '~/pages/NotePage';

export const routes: IRoutingItem[] = [
  // Главная
  {
    component: Dashboard,
    path: '/',
    exact: true,
  },
  // Страница записи
  {
    component: NotePage,
    path: '/note/:id',
    exact: true,
  },
];
