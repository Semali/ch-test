var API_URL = '/api/';

window.APP_CONFIG = Object.freeze({
  baseUrl: '/',
  apiUrl: API_URL,
  showDevInfo: false,
});
