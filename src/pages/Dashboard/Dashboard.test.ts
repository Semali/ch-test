import { testEnvironmentCreator } from '~/utilities/test-utils';
import Dashboard from '~/pages/Dashboard';

describe('<Dashboard />', () => {
  const { mount, stores, routeComponentProps } = testEnvironmentCreator();

  it('should render', () => {
    expect(mount(Dashboard, {
      ...routeComponentProps,
    }).html()).toMatchSnapshot();

    stores.user.setLocale('en');
    expect(mount(Dashboard, {
      ...routeComponentProps,
    }).html()).toMatchSnapshot();

    stores.user.setLocale('ru');
    expect(mount(Dashboard, {
      ...routeComponentProps,
    }).html()).toMatchSnapshot();
  });

  it('show modal', () => {
    const wrapper = mount(Dashboard, {
      ...routeComponentProps,
    });
    const instanceDashboard = (wrapper.find('Dashboard').instance() as Dashboard);

    (wrapper.find('button.dashboard-page__btn-create').prop('onClick') as () => {})();
    expect(instanceDashboard.showModal).toBe(true);
  });
});
