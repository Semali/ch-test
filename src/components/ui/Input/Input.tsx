import * as React from 'react';
import { FormControl } from 'react-bootstrap';
import autobind from 'autobind-decorator';
import { block } from 'bem-cn';
import { computed } from 'mobx';
import { observer } from 'mobx-react';
import { IFormField } from '~/components/FormControl';

const cn = block('app-input');

export interface IInput extends IFormField {
  value?: string;
  defaultValue?: string | number;
  inputRef?: React.RefObject<any>;
  placeholder?: string;
  maxLength?: number;
  readOnly?: boolean;
  autoComplete?: string;
}

@observer
export class Input extends React.Component<IInput> {
  private inputRefInner: React.RefObject<HTMLInputElement> = React.createRef<HTMLInputElement>();

  @computed
  private get inputRef(): React.Ref<any> {
    const { inputRef } = this.props;

    return inputRef || this.inputRefInner;
  }

  private name: string;

  constructor(props: IInput) {
    super(props);

    const { name } = this.props;

    this.name = name;
  }

  public render() {
    const {
      defaultValue,
      disabled,
      error,
      label,
      placeholder,
      value,
      autoComplete,
    } = this.props;

    return (
      <FormControl
        className={cn()}
        name={this.name}
        placeholder={placeholder}
        value={value}
        aria-label={this.name}
        onChange={this.handleChange}
        disabled={disabled}
        autoComplete={autoComplete}
        // innerRef={this.inputRef}
        ref={this.inputRef}
      />
    );
  }

  @autobind
  private handleChange(event: React.ChangeEvent<any>) {
    const { onChange, name } = this.props;
    const { target: { value } } = event;

    if (onChange) {
      onChange(value, name, event);
    }
  }
}
