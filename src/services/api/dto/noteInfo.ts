export interface ICreateNote {
  'title': string;
}

export default interface INote extends ICreateNote {
  'id': string | number;
}
