import { Locale } from '~/services/api/dto/clientInfo';
import en from './translations/en';
import ru from './translations/ru';

export const languageMap: {[key in Locale]: string} = {
  en: 'ENG',
  ru: 'RUS',
};

export type LocaleKey = keyof typeof en | keyof typeof ru | '';
