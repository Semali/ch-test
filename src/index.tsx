import { createBrowserHistory } from 'history';
import { syncHistoryWithStore } from 'mobx-react-router';
import { Provider } from 'mobx-react';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Router } from 'react-router';
import 'ts-polyfill';

import './common.scss';
import App from './components/App';
import stores from './stores';
import appConfig from './utilities/appConfig';
import { initApp } from '~/services/main';

const browserHistory = createBrowserHistory({ basename: appConfig.baseUrl });

const history = syncHistoryWithStore(browserHistory, stores.routing);

const rootId = 'root';

const AppContent: React.FunctionComponent = () => (
  <Provider {...stores}>
    <Router history={history}>
      <App />
    </Router>
  </Provider>
);

initApp(() => {
  if (process.env.NODE_ENV === 'development') {
    const { AppContainer } = require('react-hot-loader');
    const { whyDidYouUpdate } = require('why-did-you-update');

    whyDidYouUpdate(React);

    ReactDOM.render(
      <AppContainer>
        <AppContent />
      </AppContainer>,
      document.getElementById(rootId),
    );

    // https://github.com/webpack/webpack-dev-server/issues/100#issuecomment-290911036
    if ((module as any).hot) {
      (module as any).hot.accept();
    }
  } else {
    ReactDOM.render(
      <AppContent />,
      document.getElementById(rootId),
    );
  }
});
