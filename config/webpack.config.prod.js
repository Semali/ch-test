const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

// config
const publicPath = '/';

module.exports = (env) => ({
  mode: 'production',
  target: 'web',
  devtool: 'nosources-source-map',
  bail: true,
  devServer: {
    historyApiFallback: {
      disableDotRule: true
    },
    open: true,
    contentBase: './build',
    compress: true,
    port: 9000,
    stats: 'errors-only',
    proxy: {
      '/api/*': {
        target: 'http://private-9aad-note10.apiary-mock.com/',
        secure: false,
        changeOrigin: true,
        pathRewrite: {"^/api" : ""},
      },
    }
  },
  entry: [path.resolve('src/index.tsx')],
  output: {
    path: path.resolve('build'),
    publicPath,
    filename: 'static/js/[name].[chunkhash:8].js',
    chunkFilename: 'static/js/[name].[chunkhash:8].js',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.json'],
    alias: {
      '~': path.resolve('src/')
    },
    // https://github.com/andreypopp/autobind-decorator#es5-and-uglify-users
    mainFields: ['main'],
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: false,
        extractComments: true,
      })
    ],
    splitChunks: {
      chunks: 'all',
      cacheGroups: {
        vendors: {
          test: /node_modules/,
          chunks: 'all',
          reuseExistingChunk: true,
        },
        styles: {
          test: /\.css$/,
          chunks: 'all',
          reuseExistingChunk: true,
        }
      }
    }
  },
  module: {
    strictExportPresence: true,
    rules: [
      {
        oneOf: [
          {
            loader: 'babel-loader',
            test: /\.js$/,
            include: /node_modules/,
            query: {
              plugins: ['lodash'],
              presets: ['@babel/preset-env'],
            }
          },
          {
            test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/, /\.svg$/],
            loader: 'url-loader',
            options: {
              limit: 10000,
              publicPath: `${publicPath}static/media/`,
              outputPath: 'static/media/',
              name: '[name].[hash:8].[ext]',
            },
          },
          {
            test: /\.tsx?$/,
            loader: 'awesome-typescript-loader'
          },
          {
            test: /\.(sa|sc|c)ss$/,
            use: [
              {
                loader: MiniCssExtractPlugin.loader,
                options: {
                  publicPath: `${publicPath}static/css`,
                }
              },
              'css-loader',
              {
                loader: 'postcss-loader',
                options: {
                  ident: 'postcss',
                  plugins: () => [
                    require('postcss-flexbugs-fixes'),
                    autoprefixer({
                      browsers: [
                        '>1%',
                        'last 4 versions',
                        'Firefox ESR',
                        'not ie < 9',
                      ],
                      flexbox: 'no-2009',
                    }),
                  ],
                },
              },
              'sass-loader',
            ],
          },
          {
            loader: 'file-loader',
            exclude: [/\.js$/, /\.html$/, /\.json$/],
            options: {
              publicPath: `${publicPath}static/media/`,
              outputPath: 'static/media/',
              name: '[name].[hash:8].[ext]',
            },
          },
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin([path.resolve('build')], {
      root: path.resolve(__dirname , '..')
    }),
    new CopyWebpackPlugin(
      [
        { from: 'public' }
      ]
    ),
    new HtmlWebpackPlugin({
      inject: true,
      template: path.resolve('public/index.html'),
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
      },
      env: {
        publicPath,
      },
    }),
    new webpack.DefinePlugin({
      'process.env': {
        PUBLIC_PATH: JSON.stringify(publicPath),
      }
    }),
    new MiniCssExtractPlugin({
      filename: `static/css/[name].[hash].css`,
      chunkFilename: `static/css/[id].[hash].css`,
    }),
    new OptimizeCssAssetsPlugin(),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    (env && env.analyzeBuild ? new BundleAnalyzerPlugin() : () => {}),
  ],
  node: {
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty',
  }
});
