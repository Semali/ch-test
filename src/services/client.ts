import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';

import appConfig from '~/utilities/appConfig';
import { toQueryString } from '~/utilities/queryString';

axios.defaults.baseURL = appConfig.apiUrl;
axios.defaults.withCredentials = true;

class ApiError extends Error {
  public readonly status: number;
  public readonly description: any;

  constructor(error: AxiosError) {
    super('API request failed');

    this.status = error.response ? error.response.status : undefined;
    this.description = error.response ? error.response.data : undefined;

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ApiError);
    }
  }
}

export function requestInterceptor(onSuccess: (resp: AxiosResponse) => Promise<any>,
                                   onError: (err: AxiosError) => Promise<any>): number {
  return axios.interceptors.response.use(onSuccess, onError);
}

export async function request<T>(config: AxiosRequestConfig): Promise<T> {
  let response: AxiosResponse<T> = null;

  try {
    response = await axios.request<T>({...config, paramsSerializer: toQueryString});
  } catch (error) {
    // Если это cancel - не надо ее преобразовывать в ApiError, иначе axios.isCancel()
    // в сторах всегда будет возвращать false
    if (!axios.isCancel(error)) {
      throw new ApiError(error);
    } else {
      throw error;
    }
  }

  return response ? response.data : undefined;
}
