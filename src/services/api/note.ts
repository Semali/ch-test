import INote, { ICreateNote } from '~/services/api/dto/noteInfo';
import api from '~/constants/api';
import { request } from '~/services/client';

export function getList() {
  return request<INote[]>({
    method: 'get',
    url: api.notes.create,
  });
}

export function createNote(note: ICreateNote) {
  return request<INote>({
    data: note,
    method: 'post',
    url: api.notes.create,
  });
}

export function getNote(id: number | string) {
  return request<INote>({
    method: 'get',
    url: api.notes.edit(id),
  });
}

export function editNote(note: INote) {
  return request<INote>({
    data: {title: note.title},
    method: 'put',
    url: api.notes.edit(note.id),
  });
}

export function deleteNote(id: number | string) {
  return request({
    method: 'delete',
    url: api.notes.edit(id),
  });
}
